# Use the official Odoo image from Docker Hub
FROM odoo:17

# Set environment variables
ENV ODOO_RC /etc/odoo/odoo.conf

# Expose Odoo ports
EXPOSE 8069

# Change the working directory to the Odoo installation folder
WORKDIR /odoo

# Start Odoo
CMD ["odoo"]
